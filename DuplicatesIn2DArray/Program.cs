﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace DuplicatesIn2DArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter size of 2-d array, i.e number of rows and columns");
            int r=int.Parse(Console.ReadLine());
            int c=int.Parse(Console.ReadLine());
            int[,] arr = new int[r,c];
            for(int i=0;i<r;i++)
            {
                Console.WriteLine("Enter " + (i + 1) + " row values separated with space");
                string[] row = Console.ReadLine().Split();
                for(int j=0;j<c;j++)
                {
                    arr[i, j] = int.Parse(row[j]);
                }
            }

            Console.Write("Duplicate values are : ");

            var duplicates = from int v in arr group v by v into du select du;
            foreach( var v in duplicates.Select(x=>x).Where(x=>x.Count()!=1))
            {
                Console.Write(v.Key + " ");
            }
            Console.ReadKey();
        }
    }
}
